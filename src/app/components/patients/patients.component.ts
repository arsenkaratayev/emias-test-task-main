﻿import { Component, OnInit, Input } from '@angular/core';
import { ToggleAnimation } from '@app/_animations';
import { ScheduleService } from '@app/_services';

@Component({
	selector: 'app-patients',
    templateUrl: 'patients.component.html',
	animations: [ToggleAnimation],
})
export class PatientsComponent implements OnInit {
	@Input() data: any;
	
	patientsList: string = 'out';
    panelOpenState = false;
	
	selectedPatient: any = null;
	searchPatient: string = ''
	
	constructor(private readonly scheduleService: ScheduleService) { }
    
    ngOnInit() {
		if (this.data && this.data.length !== 0) {
			this.data.birthdate = new Date(this.data.birthdate);
		}
    }
	
	toggleList(event: any, item: any): void {
		this.panelOpenState = !this.panelOpenState;
		this.patientsList = this.patientsList ===  'out' ? 'in' : 'out';
	}
	
	filteredList(event: any): void {
		this.panelOpenState = true;
		this.patientsList = 'in';
		this.choosePatient(this.selectedPatient);
		this.searchPatient = '';
	}
	
	inputName(value: any) {
		/* Данная проверка здесь потому что это условие не реализовано со стороны бэкенда,
		ибо он у нас простой,повторюсь. По ТЗ было условие, что поиск начинается с ввода 3 и
		более символов. В норме ее здесь быть не должно. Сервер должен в ответ на некорректный 
		query-параметр, в данном случае слишком короткое имя пациента, сам выдавать
		либо пустой массив, либо ошибку. */
		if (value.target.value.length >= 3) {
			this.scheduleService.getAllPatients(value.target.value).subscribe((data: any) => {
				this.data = data;
			});
		}
		if (value.target.value.length < 3) {
			this.data = [];
		}
	}
	
	choosePatient(patient: any) {
		this.scheduleService.choosePatient(patient);
	}
	
	closePatient() {
		this.selectedPatient = null;
		this.choosePatient(this.selectedPatient);
		this.data = [];
	}
}