﻿import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { fadeInAnimation, ToggleAnimation } from '@app/_animations';
import { ModalService } from '@app/components/_modal';
import { ScheduleService } from '@app/_services';

@Component({
  selector: 'app-schedule-card',
  templateUrl: 'schedule-card.component.html',
  animations: [fadeInAnimation, ToggleAnimation],
  host: { '[@fadeInAnimation]': '' },
})
export class ScheduleCardComponent implements OnInit, OnDestroy {
  @Input() data: any;
  
  cell: string = 'out';
  dataModal: any;
  changedPatient: any;
  changedCell: any;
  modal: any = '';
  
  constructor(
	private modalService: ModalService,
	private readonly scheduleService: ScheduleService
  ) { }
  
  public isHidden: boolean = true;
  xPosTabMenu: number = 0;
  yPosTabMenu: number = 0;

  ngOnInit() {
	this.scheduleService.modal$.subscribe((modal: any) => {
		this.modal = modal;
		if (this.modal) {
			this.openModal(this.modal);
			this.dataModal = this.scheduleService.getPatientModal();
		}
	});
	this.scheduleService.hidden$.subscribe((modal: any) => {
		this.closeRightClickMenu()
	});
	this.scheduleService.patient$.subscribe((changedPatient: any) => {
		this.changedPatient = changedPatient;
	});
  }

  ngOnDestroy() {
	this.scheduleService.removeModalId();
  }

  weekDay(date: Date) {
    let days = ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'];
    let day = date.getDay();
    return days[day];
  }

  getMonth(date: Date) {
    let month = date.toLocaleString('default', {month: 'short'});
    return month;
  }

  notRecording(date1: any, date2: any, duration: any) {
    let count = Math.round(((date2 - date1) / 1000) / 60);
    let d = Number.parseInt(duration);
    let diff = Math.round((count / d) * 100)
    return diff
  }
  
  rightClick(event: any, item: any) {
	this.scheduleService.hiddenRightMenu(true);
    event.stopPropagation();
    this.xPosTabMenu = event.clientX;
    this.yPosTabMenu = event.clientY;
    this.isHidden = false;
	this.changedCell = item.resourceId;
	this.chooseCell(item);
    return false;
  }
  
  chooseCell(cell: any) {
	this.scheduleService.chooseCell(cell);
  }

  closeRightClickMenu() {
    this.isHidden = true;
  }
  
  openModal(id: string) {
    this.closeRightClickMenu();
    this.modalService.open(id);
	if (id === 'custom-modal-2') {
		setTimeout(() => {
			this.closeModal(id);
		}, 3000)
	}
  }
  
  deletePatient() {
    this.scheduleService.deletePatient(true);
	this.modalService.close('custom-modal-3');
  }

  closeModal(id: string) {
    this.modalService.close(id);
  }
}
